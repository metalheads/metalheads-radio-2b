<?php

use App\Http\Controllers\AlbumController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\ArtistController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\RssController;
use App\Http\Controllers\SongController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

//use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Main pages
Route::get('/', [IndexController::class, 'index'])->name('index');
Route::get('/about', [IndexController::class, 'about'])->name('about');
//Route::get('/contact', [IndexController::class, 'contact'])->name('contact');
Route::get('/contact', [ContactController::class, 'index'])->name('contact');
Route::post('/contact', [ContactController::class, 'store'])->name('contact.store');
Route::get('/article/{id}', [ArticleController::class, 'show'])->name('article.show');
Route::get("/rss", [RssController::class, "Articles"])->name('rss');

Route::middleware(['auth'])->group(function () {
//    Route::get('/profile', [ProfileController::class, 'index'])->name('profile');
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});


Route::prefix('admin')->middleware(['admin'])->group(function () {
    Route::get('/article/index', [ArticleController::class, 'index'])->name('article.index');
    Route::get('/article/create', [ArticleController::class, 'create'])->name('article.create');
    Route::post('/article/create', [ArticleController::class, 'store'])->name('article.store');
    Route::get('/article/update/{id}', [ArticleController::class, 'update'])->name('article.update');
    Route::post('/article/edit/{id}', [ArticleController::class, 'edit'])->name('article.edit');
    Route::delete('/article/delete/{id}', [ArticleController::class, 'delete'])->name('article.delete');

// CRUD user

    Route::get('/user/index', [UserController::class, 'index'])->name('user.index');
    Route::get('/user/create', [UserController::class, 'create'])->name('user.create');
    Route::post('/user/create', [UserController::class, 'store'])->name('user.store');
    Route::get('/user/update/{id}', [UserController::class, 'update'])->name('user.update');
    Route::post('/user/edit/{id}', [UserController::class, 'edit'])->name('user.edit');
    Route::get('/user/{id}', [UserController::class, 'show'])->name('user.show');
    Route::delete('/user/delete/{id}', [UserController::class, 'delete'])->name('user.delete');

// CRUD Artist

    Route::get('/artist/index', [ArtistController::class, 'index'])->name('artist.index');
    Route::get('/artist/create', [ArtistController::class, 'create'])->name('artist.create');
    Route::post('/artist/create', [ArtistController::class, 'store'])->name('artist.store');
    Route::get('/artist/update/{id}', [ArtistController::class, 'update'])->name('artist.update');
    Route::post('/artist/edit/{id}', [ArtistController::class, 'edit'])->name('artist.edit');
    Route::get('/artist/{id}', [ArtistController::class, 'show'])->name('artist.show');
    Route::delete('/artist/delete/{id}', [ArtistController::class, 'delete'])->name('artist.delete');

// CRUD Album

    Route::get('/album/index', [AlbumController::class, 'index'])->name('album.index');
    Route::get('/album/create', [AlbumController::class, 'create'])->name('album.create');
    Route::post('/album/create', [AlbumController::class, 'store'])->name('album.store');
    Route::get('/album/update/{id}', [AlbumController::class, 'update'])->name('album.update');
    Route::post('/album/edit/{id}', [AlbumController::class, 'edit'])->name('album.edit');
    Route::get('/album/{id}', [AlbumController::class, 'show'])->name('album.show');
    Route::delete('/album/delete/{id}', [AlbumController::class, 'delete'])->name('album.delete');


    // CRUD Song

    Route::get('/song/index', [SongController::class, 'index'])->name('song.index');
    Route::get('/song/create', [SongController::class, 'create'])->name('song.create');
    Route::post('/song/create', [SongController::class, 'store'])->name('song.store');
    Route::get('/song/update/{id}', [SongController::class, 'update'])->name('song.update');
    Route::post('/song/edit/{id}', [SongController::class, 'edit'])->name('song.edit');
    Route::delete('/song/delete/{id}', [SongController::class, 'delete'])->name('song.delete');
});

require __DIR__ . '/auth.php';
