<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(Request): (Response|RedirectResponse) $next
     * @return Response|RedirectResponse
     */
    public function handle(Request $request, Closure $next): Response|RedirectResponse
    {
        if (!auth()->check()) {
            return redirect()->route('login');
        }
        // Check if the user is an admin
        if (auth()->user()->IsAdmin === 1) {
            return $next($request);
        } else {
            abort(403);
        }
    }
}
