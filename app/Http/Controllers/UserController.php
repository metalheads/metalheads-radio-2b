<?php

namespace App\Http\Controllers;

use App\Models\user;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(): Factory|View|Application
    {
        $users = user::All();
        return view('user.index', compact('users'));
    }

    public function show($id): Factory|View|Application
    {
        $user = user::findorFail($id);
        return view('user.show', compact('user'));
    }

    public function create(): Factory|View|Application
    {

        return view('user.create');
    }

    public function store(Request $request): RedirectResponse
    {
        $user = new user();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->setCreatedAt(now());
        $user->setUpdatedAt(now());
        $user->save();
        return redirect()->route('index');
    }

    public function update($id): view
    {

        $user = user::findorfail($id)->where('id', $id)->first();
        return view('user.update', [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'email_verified_at' => $user->email_verified_at,
            'created_at' => date('Y-m-d h:m:s', strtotime($user->created_at))
        ]);
    }

    public function edit(Request $request, $id): RedirectResponse
    {
        $id = $request->id;
        $user = user::findorFail($id)->where('id', $id)->first();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->email_verified_at = $request->email_verified_at;
        $user->created_at = date('Y-m-d h:m:s', strtotime($request->created_at));
        $user->updated_at = now();
        $user->save();
        return redirect()->route('user.index')->with('success', 'Article mis à jour avec succès');
    }

    public function delete($id): RedirectResponse
    {
        $user = user::findorFail($id)
            ->where('id', $id)
            ->first();
        $user->delete();
        return redirect()->route('user.index');
    }
}
