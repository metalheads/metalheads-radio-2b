<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\Category;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ArtistController extends Controller
{
    public function index(): Factory|View|Application
    {
        $artists = Artist::All();
        return view('artist.index', compact('artists'));
    }

    public function show($id): Factory|View|Application
    {
        $artist = artist::findorFail($id);
        return view('artist', compact('artist'));
    }

    public function create(): Factory|View|Application
    {

        $categories = Category::All();
        return view('artist.create', compact('categories'));
    }

    public function store(Request $request): RedirectResponse
    {
        $artist = new artist();
        $artist->name = $request->name;
        $artist->Website = $request->website;
        $artist->facebook = $request->facebook;
        $artist->twitter = $request->twitter;
        $artist->youtube = $request->youtube;
        $artist->instagram = $request->instagram;
        $artist->created_at = now();
        $artist->updated_at = now();
        $artist->save();
        return redirect()->route('artist.index');
    }

    public function update($id): view
    {
        $artist = artist::findorfail($id)->where('id', $id)->first();
        return view('artist.update', compact('artist'), [
            'id' => $artist->id,
            'title' => $artist->title,
            'content' => $artist->content,
            'resume' => $artist->resume,
            'category' => $artist->category_id,
            'image' => $artist->image,
            'slug' => $artist->slug,
            'date' => date('d/m/Y', strtotime($artist->created_at)),
            'actif' => $artist->enabled,
        ]);
    }

    public function edit(Request $request, $id): RedirectResponse
    {
        $id = $request->id;
        $artist = artist::findorFail($id)->where('id', $id)->first();
        $artist->title = $request->titre;
        ($request->actif = 'on') ? $artist->enabled = 1 : $artist->enabled = 0;
        $artist->resume = $request->resume;
        $artist->content = $request->contenu;
        $artist->category_id = 1;
        $artist->slug = $request->slug;
        $artist->image = $request->image;
        $artist->created_at = $request->date;
        $artist->updated_at = now();
        $artist->save();
        return redirect()->route('artist.index');
    }

    public function delete($id): RedirectResponse
    {    // Retrieve the artist with the given ID
        $artist = artist::findorFail($id)->where('id', $id)->first();

        // Find the artist with the given ID
        // If the artist is not found, throw an exception
        $artist = artist::findorFail($id)

            // Ensure that the artist has the given ID
            ->where('id', $id)

            // Retrieve the first artist found
            ->first();
        $artist->delete();
        return redirect()->route('artist.index');
    }
}
