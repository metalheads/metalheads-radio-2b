<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Artist;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AlbumController extends Controller
{
    public function index(): Factory|View|Application
    {
        $albums = Album::select('albums.*', 'artists.name as artist_name')
            ->join('artists', 'albums.artist_id', '=', 'artists.id')
            ->get();
        return view('album.index', compact('albums'));
    }

    public function show($id): Factory|View|Application
    {
        $album = Album::findorFail($id);
        return view('article', compact('album'));
    }

    public function create(): Factory|View|Application
    {
        $artists = Artist::All();
        $nbArtist = DB::table('artists')->count();
        return view('album.create', compact('artists', 'nbArtist'));
    }

    public function store(Request $request): RedirectResponse
    {
        $album = new Album();

        $album->name = $request->name;
        $album->artist_id = $request->artist;
        $album->created_at = now();
        $album->updated_at = now();
        $album->save();
        return redirect()->route('album.index');
    }

    public function update($id): view
    {
//        $nbArtist = DB::table('artists')->count();
        $album = Album::findorfail($id)->where('id', $id)->first();
        $nbSong = DB::table('songs')->where('album_id', $album->id)->count();
        $artists = Artist::All();
//        $artists = DB::table("artists")->whereNot('id', $album->artist_id)->first();
        return view('album.update', [
            'id' => $album->id,
            'name' => $album->name,
            'songs' => $album->songs,
            'artist' => $album->artist->name,
            'artist_id' => $album->artist_id,
            'artists' => $artists,
            'nbSong' => $nbSong
        ]);
    }

    public function edit(Request $request, $id): RedirectResponse
    {
        $id = $request->id;
        $album = Album::findorFail($id)->where('id', $id)->first();
        $album->name = $request->name;
        $album->artist_id = $request->artist;
        $album->updated_at = now();
        $album->save();
        return redirect()->route('album.index');
    }

    public function delete($id): RedirectResponse
    {
        $album = Album::findorFail($id)->where('id', $id)->first();

        $album = Album::findorFail($id)
            ->where('id', $id)
            ->first();
        $album->delete();
        return redirect()->route('album.index');
    }
}
