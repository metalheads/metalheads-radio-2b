<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends Controller
{
    public function index(Request $request): Factory|View|Application
    {
        if (Auth::user()) {
            $userId = Auth::User()->getAuthIdentifier();
            $userRole = User::findorFail($userId)->where('id', $userId)->first()->IsAdmin;
        } else {
            $userId = null;
            $userRole = null;
        }
        $articles = Article::where('enabled', 1)->latest()->take(6)->get();
        return view('index', compact('articles', 'userId', 'userRole'));
    }

    public function contact(): Factory|View|Application
    {
        return view('contact');
    }

    public function about(): Factory|View|Application
    {
        return view('about');
    }
}

