<?php

namespace App\Http\Controllers;

use App\Models\Article;

class RssController extends Controller
{
    // Le flux RSS des articles
    public function Articles()
    {

        // On récupère les cours
        $articles = Article::select('title', 'resume', 'content', 'image', "created_at")
            ->latest()
            ->get();

        // La réponse avec la vue et le header spécifique
        return response()
            ->view("rss", compact("articles"))
            ->header('Content-Type', 'application/xml');
    }
}
