<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Song;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SongController extends Controller
{
    public function index(): Factory|View|Application
    {
        $songs = Song::All();
        return view('song.index', compact('songs'));
    }

    public function create(): Factory|View|Application
    {
        $albums = Album::All();
        $nbAlbums = DB::table('albums')->count();
        return view('song.create', compact('albums', 'nbAlbums'));
    }

    public function store(Request $request): RedirectResponse
    {
        $album = new Song();
        $album->title = $request->title;
        $album->number = $request->number;
        $album->songtime = $request->songtime;
        $album->album_id = $request->album;
        $album->created_at = now();
        $album->updated_at = now();
        $album->save();
        return redirect()->route('song.index');
    }

    public function update($id): view
    {
        $nbAlbum = DB::table('albums')->count();
        $song = Song::findorfail($id)->where('id', $id)->first();
        $albums = Album::All();
        return view('song.update', [
            'id' => $song->id,
            'title' => $song->title,
            'number' => $song->number,
            'songtime' => $song->songtime,
            'albums' => $albums,
            'album' => $song->album->name,
            'album_id' => $song->album_id,
            'nbAlbum' => $nbAlbum,
        ]);
    }

    public function edit(Request $request, $id): RedirectResponse
    {
        $id = $request->id;
        $album = Album::findorFail($id)->where('id', $id)->first();
        $album->name = $request->name;
        $album->artist_id = $request->artist;
        $album->updated_at = now();
        $album->save();
        return redirect()->route('song.index');
    }

    public function delete($id): RedirectResponse
    {
        $song = Song::findorFail($id)->where('id', $id)->first();

        $song = Song::findorFail($id)
            ->where('id', $id)
            ->first();
        $song->delete();
        return redirect()->route('song.index');
    }
}
