<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use App\Models\Image;
use App\Models\Paragraph;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index(): Factory|View|Application
    {
        $articles = Article::select('articles.*', 'categories.libelle as category_name')
            ->join('categories', 'articles.category_id', '=', 'categories.id')
            ->get();

        return view('article.index', compact('articles'));
    }

    public function show($id): Factory|View|Application
    {
        $article = Article::findorFail($id);
        $images = Image::where('article_id', $article->id)->get();
        $array_images = [];
        foreach ($images as $image) {
            array_push($array_images, $image->name);
        }
        $nbImages = sizeof($array_images);


        $paragraphs = Paragraph::where('article_id', $article->id)->get();
        $array_paragraphs= [];
        foreach ($paragraphs as $paragraph) {
            array_push($array_paragraphs, $paragraph->content);
        }
        $nbParagraphs = sizeof($array_images);
        return view('article', compact('article', 'array_images', 'nbImages', 'array_paragraphs', 'nbParagraphs'));
    }

    public function create(): Factory|View|Application
    {
        $today = date('Y-m-d');
        $categories = Category::All();
        return view('article.create', compact('categories', 'today'));
    }

    public function store(Request $request): RedirectResponse
    {
        $article = new Article();
        $article->title = $request->titre;
        $article->enabled = $request->has('enabled');
        $article->resume = $request->resume;
        $article->content = $request->contenu;
        $article->category_id = $request->category;
        $article->slug = $request->slug;
        $article->image = $request->image;
        $article->created_at = $request->date;
        $article->updated_at = now();
        $article->save();
        return redirect()->route('index');
    }

//    @TODO: Add paragraphs and images in their corresponding tables ( task added in notion )

    public function update($id): view
    {
        $categories = Category::All();
        $article = Article::findorfail($id)->where('id', $id)->first();
        $created_at = $article->created_at->format('Y-m-d');
        return view('article.update', compact('categories', 'article', 'created_at'),
            ['category' => $article->category_id]);
    }

    public function edit(Request $request, $id): RedirectResponse
    {
        $id = $request->id;
        $article = Article::findorFail($id)->where('id', $id)->first();
        $article->title = $request->title;
        $article->enabled = $request->has('enabled');
        $article->resume = $request->resume;
        $article->content = $request->contenu;
        $article->category_id = 1;
        $article->slug = $request->slug;
        $article->image = $request->image;
        $article->created_at = $request->date;
        $article->updated_at = now();
        $article->save();
        return redirect()->route('article.index');
    }

    public function delete($id): RedirectResponse
    {
        $article = Article::findorFail($id)->where('id', $id)->first();
        $article->delete();
        return redirect()->route('article.index');
    }
}
