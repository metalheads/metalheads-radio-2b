<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function index(Request $request): Factory|View|Application
    {
        return view('contact');
    }

    public function store(Request $request): RedirectResponse
    {
        $request->validate([ 'name' => 'required', 'email' => 'required|email', 'message' => 'required' ]);
    Contact::create($request->all());

    Mail::send('email',
        array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'user_message' => $request->get('message')
        ), function($message)
        {
            $message->from('perinog@gmail.com');

            $message->to('gabriel.hamadache@gmail.com', 'Admin')->subject('Metalheads feedback');
        });
        return back()->with('success', 'Thanks for contacting us!');
    }

}

