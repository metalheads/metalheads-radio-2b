<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Article extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'resume', 'content', 'category_id', 'slug', 'image', 'enabled', 'created_at', 'updated_at'];

    public function artists(): BelongsToMany
    {
        return $this->belongsToMany(Artist::class);
    }

    public function category(): BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }
}
