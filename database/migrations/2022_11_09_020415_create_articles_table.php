<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->boolean('enabled')->default(1);
            $table->mediumText('resume');
            $table->LongText('content');
            $table->foreignId('category_id')->constrained();
            $table->mediumText('image');
            $table->mediumText('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->dropForeign(['category_id']);
            $table->dropForeign(['artist_id']);
            $table->dropColumn(['category_id']);
            $table->dropColumn(['artist_id']);
        });
        Schema::dropIfExists('articles');
    }
};
