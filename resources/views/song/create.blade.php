@extends('layouts.base')
@section('content')
    <form method="POST" action="{{route('song.store')}}">
        @csrf
        <table>
            <tr>

                <td>Titre</td>
                <td>
                    <label for="title">
                        <input type="text" id="title" name="title">
                    </label>
                </td>

            </tr>
            <tr>
                <td>Album</td>
                @if ($nbAlbums > 0)
                    <td>
                        <label for="album">
                            <select name="album" id="album">
                                <option value="problem"> ---- VEUILLEZ CHOISIR UNE OPTION ----</option>
                                @foreach($albums as $album)
                                    <option value="{{$album->id}}">{{$album->name}}</option>
                                @endforeach
                            </select>
                        </label>
                    </td>
                @else
                    <td><a href="{{ url('/admin/album/create') }}">Créer un album</a></td>
                @endif

            </tr>
            <tr>
                <td>Number</td>
                <td>
                    <label for="number">
                        <input name="number" id="number" min='1' max="25" type="number">
                    </label>
                </td>
            </tr>
            <tr>
                <td>songtime</td>
                <td>
                    <label for="songtime">
                        <input name="songtime" id="songtime" type="time" step="2">
                    </label>
                </td>
            </tr>
        </table>
        <button name="BtnConfirm">Envoyer</button>
    </form>
@endsection
