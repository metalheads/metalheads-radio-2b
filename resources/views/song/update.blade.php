@extends('layouts.base')
@section('content')
    <form method="POST" action="{{url('/admin/album/edit/'. $id)}}">
        @csrf
        <table>
            <tr>
                <td>titre</td>
                <td>
                    <label for="title">
                        <input type="text" name="title" value="{{$title}}">
                    </label>
                </td>
            </tr>

            <tr>
                <td>numéro dans l'album</td>
                <td>
                    <label for="number">
                        <input type="number" name="number" min="1" value="{{$number}}">
                    </label>
                </td>
            </tr>

            <tr>
                <td>songtime</td>
                <td>
                    <label for="songtime">
                        <input type="time" step="2" name="songtime" value="{{$songtime}}">
                    </label>
                </td>
            </tr>
            <tr>
                <td>Album</td>
                <td>
                    <label for="album">
                        <select name="album" id="album">
                            <option selected value="{{$album_id}}"> {{$album}}</option>
                            @foreach($albums as $album)
                                <option value="{{$album->id}}">{{$album->name}}</option>
                            @endforeach
                        </select>
                    </label>
                </td>
            </tr>

        </table>
        <button name="BtnConfirm">Envoyer</button>
    </form>
@endsection
