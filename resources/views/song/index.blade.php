@extends('layouts.base')
@section('content')

    {{--@TODO:  Add many filter (category, name (a-z / z-a), etc...--}}


    <table class="albums-list">
        {{--        <tr>--}}
        {{--            <td colspan="3" style="text-align: right">--}}
        {{--                --}}{{--@TODO:  create a input to search songs--}}
        {{--                    <label for="search">--}}
        {{--                        <input type="text" id="search" name="search" placeholder="Search an album">--}}
        {{--                    </label>--}}
        {{--            </td>--}}
        {{--        </tr>--}}
        <tr>
            <td>Titre</td>
            <td>Numéro</td>
            <td>Temps</td>
            <td>Album</td>
            <td style="text-align: center;">Action</td>
        </tr>

        @foreach($songs as $song)
            <tr>
                <td>
                    <a href="{{route('song.update', $song->id)}}">{{$song->title}}</a>
                </td>
                <td>
                    <p>{{$song->number}}</p>
                </td>
                <td>
                    <p>{{$song->songtime}}</p>
                </td>
                <td>
                    <p>{{$song->album_id}}</p>
                </td>
                <td>
                    <a href="{{route('song.update', $song->id)}}">
                        <span>
                            <i class="fas fa-fw fa-edit"></i>
                        </span>Éditer
                    </a>
                    <a href="{{route('song.delete', $song->id)}}">
                        <span>
                            <i class="fas fa-fw fa-trash"></i>
                        </span>supprimer
                    </a>
                </td>
            </tr>
        @endforeach
    </table>
    <a href="{{route('admin.song.create')}}">
        <i class="fas fa-fw fa-plus"></i>
        Ajouter
    </a>
@endsection
