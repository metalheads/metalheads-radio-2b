{{--//COPYRIGHT (C)  metalheads_laravel ( Gabriel PERINO) 2022. All rights reserved.--}}
@extends('layouts.base')
@section('content')
{{--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}
    <div class="contact">
        <h1>Nous contacter</h1>

        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        @endif
        {!! Form::open(['route'=>'contact.store']) !!}

        <div class="form-container">
            {!! Form::open(['route'=>'contact.store']) !!}
            <div class="labels">
                {!! Form::label('Name:') !!}
                {!! Form::label('Email:') !!}
            </div>
            <div class="inputs">
                {!! Form::text('name', old('name'), ['class'=>'form-control', 'placeholder'=>'Enter Name']) !!}
                <span class="text-danger">{{ $errors->first('name') }}</span>

                {!! Form::text('email', old('email'), ['class'=>'form-control', 'placeholder'=>'Enter Email']) !!}
                <span class="text-danger">{{ $errors->first('email') }}</span>
            </div>
            <div class="labels">
                {!! Form::label('Message:') !!}
            </div>
            <div class="textarea">
            {!! Form::textarea('message', old('message'), ['class'=>'form-control', 'placeholder'=>'Enter Message']) !!}
            <span class="text-danger">{{ $errors->first('message') }}</span>
            </div>


            <div class=" form-group btn-container">
                <button type="submit">Envoyer</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
