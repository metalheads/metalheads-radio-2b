{{--COPYRIGHT (C)  metalheads_laravel ( Gabriel PERINO) 2022. All rights reserved.--}}

<html lang="fr">
<head>
    <link rel="icon" href="{{asset('/assets/images/favicon.ico')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('/assets/css/main.css')}}"/>
    <title>Blog des metalheads</title>
    <meta charset="UTF-8">
</head>
<body>

<aside>
    <div class="icon">
        <a href="{{route('index')}}">
            <img src="{{asset("assets/images/logo-blanc.svg")}}" alt="logo-svg">
        </a>
    </div>
    <div class="link-container">
        <a class="link" href="{{route('index')}}">Accueil</a>
        {{--        <a class="link" href="{{route('contact')}}">Recherche</a>--}}
        <a class="link" href="{{route('about')}}">A propos</a>
        <a class="link" href="{{route('contact')}}">Contact</a>
    </div>
</aside>
@yield('content')
<script src="https://kit.fontawesome.com/0443606be6.js" crossorigin="anonymous"></script>
</body>
</html>
