@extends('layouts.base')
@section('content')
    <form method="POST" action="{{route('artist.store')}}">
        @csrf
        <table>
            <tr>

                <td>Nom</td>
                <td>
                    <label for="name">
                        <input type="text" name="name" id="name">
                    </label>
                </td>

            </tr>
            <tr>
                <td>Site internet</td>
                <td>
                    <label for="website">
                        <input type="text" name="website" id="website" value="https://www.test.com">
                    </label>
                </td>
            </tr>
            <tr>
                <td>facebook</td>
                <td>
                    <label for="facebook">
                        <input type="text" name="facebook" id="facebook" value="https://www.facebook.com">
                    </label>
                </td>
            </tr>
            <tr>
                <td>twitter</td>
                <td>
                    <label for="twitter">
                        <input type="text" name="twitter" id="twitter" value="https://www.twitter.com">
                    </label>
                </td>
            </tr>
            <tr>
                <td>youtube</td>
                <td>
                    <label for="youtube">
                        <input type="text" name="youtube" id="youtube" value="https://www.youtube.com">
                    </label>
                </td>
            </tr>
            <tr>
                <td>instagram</td>
                <td>
                    <label for="instagram">
                        <input type="text" name="instagram" id="instagram" value="https://www.instagram.com">
                    </label>
                </td>
            </tr>
        </table>
        <button type="submit" name="BtnConfirm" value="Envoyer">Envoyer</button>
    </form>
@endsection
