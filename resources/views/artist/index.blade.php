@extends('layouts.base')
@section('content')

    {{--@TODO:  Add many filter (category, name (a-z / z-a), etc...--}}


    <table class="artists-list">
        {{--        <tr>--}}
        {{--            <td colspan="3" style="text-align: right">--}}
        {{--                --}}{{--@TODO:  create a input to search artist--}}
        {{--                    <label for="search">--}}
        {{--                        <input type="text" id="search" name="search" placeholder="Search an artist">--}}
        {{--                    </label>--}}
        {{--            </td>--}}
        {{--        </tr>--}}
        <tr>
            <td>name</td>
            <td>website</td>
            <td>facebook</td>
            <td>twitter</td>
            <td>youtube</td>
            <td>instagram</td>
            <td style="text-align: center;">Action</td>
        </tr>

        @foreach($artists as $artist)
            <tr>
                <td>
                    <a href="{{url('/admin/artist/'. $artist->id)}}">{{$artist->name}}</a>
                </td>
                <td>
                    <p>{{$artist->website}}</p>
                </td>
                <td>
                    <p>{{$artist->facebook}}</p>
                </td>
                <td>
                    <p>{{$artist->twitter}}</p>
                </td>
                <td>
                    <p>{{$artist->youtube}}</p>
                </td>
                <td>
                    <p> {{$artist->instagram}}</p>
                </td>
                <td>
                    <a href="{{url('/admin/artist/update/'. $artist->id)}}">
                        <span>
                            <i class="fas fa-fw fa-edit"></i>
                        </span>Éditer
                    </a>
                    <a href="{{url('/admin/artist/delete/'. $artist->id)}}">
                        <span>
                            <i class="fas fa-fw fa-trash"></i>
                        </span>supprimer
                    </a>
                </td>
            </tr>
        @endforeach
    </table>
    <a href="{{url('/admin/artist/create')}}">
        <i class="fas fa-fw fa-plus"></i>
        Ajouter
    </a>
@endsection
