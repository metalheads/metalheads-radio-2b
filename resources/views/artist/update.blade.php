@extends('layouts.base')
@section('content')
    <form method="POST" action="{{url('artist/edit/'. $id)}}">
        @csrf
        <table>
            <tr>

                <td>Nom</td>
                <td>
                    <label for="name">
                        <input type="text" name="name" id="name">
                    </label>
                </td>

            </tr>
            <tr>
                <td>Site internet</td>
                <td>
                    <label for="website">
                        <input type="text" name="website" id="website" value="{{$artist->website}}">
                    </label>
                </td>
            </tr>
            <tr>
                <td>facebook</td>
                <td>
                    <label for="facebook">
                        <input type="text" name="facebook" id="facebook" value="{{$artist->facebook}}">
                    </label>
                </td>
            </tr>
            <tr>
                <td>twitter</td>
                <td>
                    <label for="twitter">
                        <input type="text" name="twitter" id="twitter" value="{{$artist->twitter}}">
                    </label>
                </td>
            </tr>
            <tr>
                <td>youtube</td>
                <td>
                    <label for="youtube">
                        <input type="text" name="youtube" id="youtube" value="{{$artist->youtube}}">
                    </label>
                </td>
            </tr>
            <tr>
                <td>instagram</td>
                <td>
                    <label for="instagram">
                        <input type="text" name="instagram" id="instagram" value=" {{$artist->instagram}}">
                    </label>
                </td>
            </tr>
        </table>
        <button type="submit" name="BtnConfirm" value="Envoyer">Envoyer</button>
    </form>
@endsection
