@extends('layouts.base')
@section('content')
    <form method="POST" action="{{route('album.store')}}">
        @csrf
        <table>
            <tr>

                <td>Nom</td>
                <td>
                    <label for="name">
                        <input type="text" name="name">
                    </label>
                </td>

            </tr>
            <tr>
                <td>Artiste</td>
                @if ($nbArtist > 0)
                    <td>
                        <label for="artist">
                            <select name="artist" id="artist">
                                <option value="problem"> ---- VEUILLEZ CHOISIR UNE OPTION ----</option>
                                @foreach($artists as $artist)
                                    <option value="{{$artist->id}}">{{$artist->name}}</option>
                                @endforeach
                            </select>
                        </label>
                    </td>
                @else
                    <td><a href="{{ url('/admin/artist/create') }}">Créer un artiste</a></td>
                @endif
            </tr>
        </table>
        <button name="BtnConfirm">Envoyer</button>
    </form>
@endsection
