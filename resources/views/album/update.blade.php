@extends('layouts.base')
@section('content')
    <form method="POST" action="{{url('/admin/album/edit/'. $id)}}">
        @csrf
        <h2>Album</h2>
        <table>
            <tr>

                <td>name</td>
                <td>
                    <label for="name">
                        <input type="text" name="name" value="{{$name}}">
                    </label>
                </td>
            </tr>
            <tr>
                <td>Artiste</td>
                <td>
                    <label for="artist">
                        <select name="artist" id="artist">
                            <option selected value="{{$artist_id}}"> {{$artist}}</option>
                            @foreach($artists as $artist)
                                <option value="{{$artist->id}}">{{$artist->name}}</option>
                            @endforeach
                        </select>
                    </label>
                </td>
            </tr>

        </table>
        <button name="BtnConfirm">Envoyer</button>
    </form>
    <h2>Songs</h2>
    @if ($nbSong >0)
        <table>
            <tr>
                <td>title</td>
                <td>number</td>
                <td>songtime</td>
                <td>album_id</td>
            </tr>
            <tr>
                @foreach($songs as $song)
                    <td><p>{{$song->title}}</p></td>
                    <td><p>{{$song->number}}</p></td>
                    <td><p>{{$song->songtime}}</p></td>
                    <td><p>{{$song->album_id}}</p></td>
                @endforeach
            </tr>
        </table>
    @else
        <a href="{{url('/admin/song/create')}}">Ajouter des chansons</a>
    @endif
@endsection
