@extends('layouts.base')
@section('content')

    {{--@TODO:  Add many filter (category, name (a-z / z-a), etc...--}}


    <table class="albums-list">
        {{--        <tr>--}}
        {{--            <td colspan="3" style="text-align: right">--}}
        {{--                --}}{{--@TODO:  create a input to search albums--}}
        {{--                    <label for="search">--}}
        {{--                        <input type="text" id="search" name="search" placeholder="Search an album">--}}
        {{--                    </label>--}}
        {{--            </td>--}}
        {{--        </tr>--}}
        <tr>
            <td>Nom</td>
            <td>Artiste</td>
            <td style="text-align: center;">Action</td>
        </tr>

        @foreach($albums as $album)
            <tr>
                <td>
                    <p>{{$album->name}}</p>
                </td>
                <td>
                    <p>{{$album->artist_name}}</p>
                </td>
                <td>
                    <a href="{{route('album.update', $album->id)}}">
                        <span>
                            <i class="fas fa-fw fa-edit"></i>
                        </span>Éditer
                    </a>
                    <a href="{{route('album.delete', $album->id)}}">
                        <span>
                            <i class="fas fa-fw fa-trash"></i>
                        </span>supprimer
                    </a>
                </td>
            </tr>
        @endforeach
    </table>
    <a href="{{route('album.create')}}">
        <i class="fas fa-fw fa-plus"></i>
        Ajouter
    </a>
@endsection
