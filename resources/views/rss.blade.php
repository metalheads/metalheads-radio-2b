@php echo '<?xml version="1.0" encoding="UTF-8"?>' @endphp
<rss version="2.0">
    <channel>
        <title>{{ config('app.name') }}</title>
        <description>Description de mon flux RSS ici</description>
        <lastBuildDate>{{ $articles->first()->created_at->toRfc7231String() }}</lastBuildDate>
        <link>{{ config('app.url') }}</link>
        @foreach ($articles as $article)
            <item>
                <title>{{ $article->title }}</title>
                <description>{{ $article->resume }}</description>
                <pubDate>{{ $article->created_at->toRfc7231String() }}</pubDate>
                {{--                <link>{{url('/article/'.$article->id)}}</link>--}}
                {{--                <link>{{ route('article.show', $article->id) }}</link>--}}
            </item>
        @endforeach
    </channel>
</rss>
