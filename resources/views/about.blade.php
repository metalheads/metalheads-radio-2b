{{--//COPYRIGHT (C)  metalheads_laravel ( Gabriel PERINO) 2022. All rights reserved.--}}
@extends('layouts.base')
@section('content')
    <h1>A propos de nous </h1>
    <h2> lieu d'émission</h2>
    <p>
        Notre groupe de Radio se réunissait pour une émission environ les deux mois afin d'avoir
        le temps de préparer correctement le contenu et l'organisation de l'émission.
    </p>
        <h2>Présentation des membres du groupe </h2>
        <img class="img-one-metalheads" src="{{asset('assets/images/neils.jpg')}}" alt="photo de Neils">
        <img class="img-one-metalheads" src="{{asset('assets/images/Mattheo.png')}}" alt="photo de Matthéo">
        <img class="img-one-metalheads" src="{{asset('assets/images/Julie.png')}}" alt="photo de Julie">
        <img class="img-one-metalheads" src="{{asset('assets/images/Gabriel.png')}}" alt="photo de Gabriel">
    <br>
{{--        <h2>Moments souvenirs </h2>--}}
{{--    <br>--}}
{{--        <img class="img-two-metalheads" src="{{asset('assets/images/Matt_Gab_studio.jpg')}}" alt="photo de Gabriel et Matthéo">--}}
{{--        <img class="img-three-metalheads" src="{{asset('assets/images/emission.jpg')}}" alt="photo de Mia, Matthéo et Gabriel pendant une émission">--}}
{{--        <img class="img-four-metalheads" src="{{asset('assets/images/four_original_studio.jpg')}}" alt="photo des quatre premiers membres du groupe">--}}
{{--        <img class="img-five-metalheads" src="{{asset('assets/images/five_metalheads.jpg')}}" alt="photo du groupe pour l'accueil de Mia">--}}
@endsection
