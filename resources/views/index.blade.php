{{--//COPYRIGHT (C)  metalheads_laravel ( Gabriel PERINO) 2022. All rights reserved.--}}
@extends('layouts.base')
@section('content')
    <div class="header">
        <h1>Métal: Dernières actus</h1>

        @auth
            @if ( $userRole === 1)
                <a href="{{url('dashboard')}}">Mon compte</a>
                {{--@else--}}
                {{--    <a href="{{route('profile')}}">Mon profil</a>--}}
            @endif

        @endauth
        {{--@TODO : create a dropdown menu with different links and personalized if user is admin or user
         account.png => login / logout | user's name like dashboard page | link to admin(dashboard) if rights--}}
        {{-- @guest--}}
        {{--     <a href="{{url('login')}}">se connecter </a>--}}
        {{-- @endguest--}}

    </div>

    <div class="grid-container">
        @foreach($articles as $article)
            <div class="article-container">
                <div class="image-container">
                    <img class="article-img" src="{{asset("assets/images/".$article->image)}}" alt="image">
                </div>
                <div class="text-container">
                    <h2>
                        {{$article->title }}
                    </h2>
                </div>
                <div class="button-container">
                    <button class="btn-left"><a href="{{route('article.show', $article->id)}}">Article</a></button>
                    <button class="btn-right"><a href="{{route('article.show', $article->id)}}">Voir plus </a></button>
                </div>
            </div>
        @endforeach
    </div>
@endsection

