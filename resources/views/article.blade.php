@extends('layouts.base')
@section('content')
    <h1>{{$article->title}}</h1>
    <p>{{$article->resume}}</p>
    <img src="{{asset("assets/images/".$article->image)}}" alt="image" width="200" height="200">

    <p>{{$article->content}}</p>
{{--    @if($nbImages > 0)--}}
{{--        @for( $i = 0 ; $i < $nbImages ; $i++ )--}}
{{--            <img src="{{asset("assets/images/". $array_images[$i])}}" alt="image" width="200" height="200">--}}
{{--        @endfor--}}
{{--    @endif--}}

{{--    @if($nbParagraphs > 0)--}}
{{--        @for( $i = 0 ; $i < $nbParagraphs ; $i++ )--}}
{{--            <p> {{ $array_paragraphs[$i]}}</p>--}}
{{--        @endfor--}}
{{--    @endif--}}
@endsection
