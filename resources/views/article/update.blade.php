@extends('layouts.base')
@section('content')
    <form method="POST" action="{{route('article.edit', $article->id)}}">
        @csrf
        <table>
            <tr>

                <td>titre</td>
                <td>
                    <label for="titre">
                        <input type="text" name="title" value="{{$article->title}}">
                    </label>
                </td>

            </tr>
            <tr>
                <td>actif</td>
                <td>

                    <label class="switch">
                        <input type="checkbox"  name="enabled" id="enabled" class="enabled" {{ $article->enabled ? 'checked' : '' }}>
                        <span class="slider round"></span>
                    </label>
                </td>
            </tr>
            <tr>
                <td>resume</td>
                <td>
                    <label for="resume">
                        <input type="text" name="resume" value="{{$article->resume}}">
                    </label>
                </td>
            </tr>
            <tr>
                <td>contenu</td>
                <td>
                    <label for="contenu">
                        <textarea name="contenu" cols="100" rows="12">
                            {{$article->content}}
                        </textarea>
                    </label>
                </td>
            </tr>
            <tr>
                <td>categorie</td>
                <td>
                    <p>{{$category}}</p>
                    <label for="category">
                        <select name="category" id="category">
                            <option value="problem"> ---- VEUILLEZ CHOISIR UNE OPTION ----</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}" {{ $article->category_id == $category->id ? 'selected' : '' }}>
                                    {{ $category->libelle }}
                                </option>
                            @endforeach
                        </select>
                    </label>
                </td>
            </tr>
            <tr>
                <td>image</td>
                <td>
                    <label for="image">
                        <input type="text" name="image" value="{{$article->image}}">
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    slug
                </td>
                <td>
                    <label for="slug">
                        <input type="text" name="slug" value="{{$article->slug}}">
                    </label>
                </td>
            </tr>
            <tr>

                <td>date</td>
                <td>
                    <label for="date">
                        <input type="date" name="date" value="{{$created_at ?? '' }}">
                    </label>
                </td>
            </tr>
        </table>
        <button name="BtnConfirm">Envoyer</button>
    </form>
@endsection
