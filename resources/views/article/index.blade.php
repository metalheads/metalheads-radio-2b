@extends('layouts.base')
@section('content')

    {{--@TODO:  Add many filter (category, name (a-z / z-a), etc...--}}


    <table class="articles-list">
        {{--        <tr>--}}
        {{--            <td colspan="3" style="text-align: right">--}}
        {{--                --}}{{--@TODO:  create a input to search article--}}
        {{--                    <label for="search">--}}
        {{--                        <input type="text" id="search" name="search" placeholder="Search an article">--}}
        {{--                    </label>--}}
        {{--            </td>--}}
        {{--        </tr>--}}
        <tr>
            <td>titre</td>
            <td>slug</td>
            <td>Catégorie</td>
            <td style="text-align: center;">Action</td>
        </tr>

        @foreach($articles as $article)
            <tr>
                <td>
                    <p>{{$article->title}}</p>
                </td>                <td>
                    <a href="{{route('article.show', $article->id)}}">{{$article->slug}}</a>
                </td>
                <td>
                    <p>{{$article->category_name}}</p>
                </td>
                <td>
                    <a href="{{route('article.update',$article->id)}}">
                        <span>
                            <i class="fas fa-fw fa-edit"></i>
                        </span>Éditer
                    </a>
                </td>
                <td>
                    <form action="{{ route('article.delete', $article->id) }}" method="POST" onsubmit="return confirm('Êtes-vous sûr de vouloir supprimer cet article ?');">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">
                            <i class="fas fa-fw fa-trash"></i> Supprimer
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
    <a href="{{route('article.create')}}">
        <i class="fas fa-fw fa-plus"></i>
        Ajouter
    </a>
@endsection
