@extends('layouts.base')
@section('content')
    <form method="POST" action="{{route('article.store')}}">
        @csrf
        <table>
            <tr>

                <td>titre</td>
                <td>
                    <label for="titre">
                        <input type="text" name="titre">
                    </label>
                </td>

            </tr>
            <tr>
                <td>actif</td>
                <td>

                    <label class="switch">
                        <input type="checkbox" name="enabled" id="enabled" class="enabled" checked>
                        <span class="slider round"></span>
                    </label>
                </td>
            </tr>
            <tr>
                <td>resume</td>
                <td>
                    <label for="resume">
                        <input type="text" name="resume">
                    </label>
                </td>
            </tr>
            <tr>
                <td>contenu</td>
                <td>
                    <label for="contenu">
                        <textarea name="contenu" cols="100" rows="8"></textarea>
                    </label>
                </td>
            </tr>
            <tr>
                <td>categories</td>
                <td>
                    <label for="category">
                        <select name="category" id="category">
                            <option value="problem"> ---- VEUILLEZ CHOISIR UNE OPTION ----</option>
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->libelle}}</option>
                            @endforeach
                        </select>
                    </label>
                </td>
            </tr>
            <tr>
                <td>image</td>
                <td>
                    <label for="image">
                        <input type="file" accept="image/png" name="image">
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    slug
                </td>
                <td>
                    <label for="slug">
                        <input type="text" name="slug">
                    </label>
                </td>
            </tr>
            <tr>
                <td>date</td>
                <td>
                    <label for="date">
                        <input type="date" name="date" value="{{$today}}">
                    </label>
                </td>
            </tr>
        </table>
        <button name="BtnConfirm">Envoyer</button>
    </form>
@endsection
