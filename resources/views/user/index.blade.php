@extends('layouts.base')
@section('content')

    {{--@TODO:  Add many filter (category, name (a-z / z-a), etc...--}}


    <table class="users-list">
        {{--        <tr>--}}
        {{--            <td colspan="3" style="text-align: right">--}}
        {{--                --}}{{--@TODO:  create a input to search user--}}
        {{--                    <label for="search">--}}
        {{--                        <input type="text" id="search" name="search" placeholder="Search an user">--}}
        {{--                    </label>--}}
        {{--            </td>--}}
        {{--        </tr>--}}

        {{--        @TODO: create a password generator --}}
        <tr>
            <td>name</td>
            <td>email</td>
            <td style="text-align: center;">Action</td>
        </tr>

        @foreach($users as $user)
            <tr>
                <td>
                    <a href="{{route('user.show',$user->id)}}">{{$user->name}}</a>
                </td>
                <td>
                    <p>{{$user->email}}</p>
                </td>
                <td>
                    <a href="{{route('user.update', $user->id)}}">
                        <span>
                            <i class="fas fa-fw fa-edit"></i>
                        </span>Éditer
                    </a>
                    <a href="{{route('user.delete', $user->id)}}">
                        <span>
                            <i class="fas fa-fw fa-trash"></i>
                        </span>supprimer
                    </a>
                </td>
            </tr>
        @endforeach
    </table>
@endsection
