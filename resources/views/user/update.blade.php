@extends('layouts.base')
@section('content')
    <form method="POST" action="{{route('user.edit',$id)}}">
        @csrf
        <table>
            <tr>

                <td>name</td>
                <td>
                    <label for="name">
                        <input type="text" name="name" value="{{$name}}">
                    </label>
                </td>

            </tr>
            <tr>
                <td>email</td>
                <td>
                    <label for="email">
                        <input type="text" name="email" value="{{$email}}">
                    </label>
                </td>
            </tr>
            <tr>
                <td>email_verified_at</td>
                <td>
                    <label for="email_verified_at">
                        <input type="date" name="email_verified_at" id="email_verified_at" value="{{$email_verified_at}}">
                    </label>
                </td>
            </tr>
            <tr>
                <td>created_at</td>
                <td>
                    <label for="created_at">
                        <input type="datetime-local" name="created_at" id="created_at" value="{{$created_at}}">
                    </label>
                </td>
            </tr>
        </table>
        <button name="BtnConfirm">Envoyer</button>
    </form>
@endsection
